const Sequelize = require('sequelize');
const sequelize=require('./db');
    const Grupa = sequelize.define('Grupa', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
          },
      naziv : Sequelize.STRING
  },{
      freezeTableName: true
  },{
      timestamps: false
  });

  module.exports =Grupa;