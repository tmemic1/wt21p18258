const Sequelize = require('sequelize');
const sequelize=require('./db');
    const Student = sequelize.define('Student', {
      ime : Sequelize.STRING,
      prezime : Sequelize.STRING,
      indeks : Sequelize.INTEGER,
      grupa_id : Sequelize.INTEGER,
  },{
      freezeTableName: true
  },{
      timestamps: false
  });
module.exports=Student;