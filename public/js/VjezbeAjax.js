let VjezbeAjax = (function () {
      var biloVjezbi=0;
    function dodajInputPolja(DOMelementDIVauFormi,brojVjezbi) {
      if(brojVjezbi<0 || brojVjezbi>15) {
        return;
      }
      var div,vrijednosti,x;
      if(biloVjezbi<0 || biloVjezbi>15) return;
        for(let i=0; i<brojVjezbi; i++) {
           div=document.createElement("div");
           vrijednosti=document.createElement("vrijednosti");
           x = document.createElement("INPUT");
        vrijednosti.innerHTML="Broj zadataka za vježbu "+(i+1)+":<br>"; 
        x.setAttribute("type", "text");
        x.setAttribute("value", 4);
        x.setAttribute("name", "z"+i);
        x.setAttribute("id", "z"+i);
        div.appendChild(vrijednosti);
        div.appendChild(x);
        DOMelementDIVauFormi.appendChild(div);
         }
      }

      function posaljiPodatke(vjezbeObjekat,callbackFja){
        var ajax = new XMLHttpRequest();
        ajax.open("POST","http://localhost:3000/vjezbe",true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null,ajax.response);
            }
            else if (ajax.readyState == 4) 
              callbackFja(ajax.response,null);
        }
        
        ajax.send(JSON.stringify(vjezbeObjekat));
     }

     var dohvatiPodatke = function (callbackFja) { 
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) { 
              var jsonres=JSON.parse(xhttp.responseText);           
              callbackFja(null, jsonres);
             }
           if(xhttp.readyState == 4 && xhttp.status == 404){
               callbackFja(xhttp.response, null);
             }
      };
      xhttp.open("GET", "/vjezbe", true);
      xhttp.send();
  };

     function iscrtajVjezbe(divDOMelement,objekat) {
     for(var i=0; i<objekat.brojVjezbi; i++) {
       var divZaUbacit=document.createElement("div");
       divZaUbacit.classList.add("vjezbe-lista","Vjezba"+(i+1));
       let divel=document.createElement("button");
       divel.classList.add("Vjezba");
       divel.innerHTML="Vjezba "+(i+1);
       divZaUbacit.appendChild(divel);
       divDOMelement.appendChild(divZaUbacit);
        }
     }
      return {dodajInputPolja:dodajInputPolja, posaljiPodatke:posaljiPodatke, dohvatiPodatke:dohvatiPodatke,iscrtajVjezbe:iscrtajVjezbe};    
    } ());