let TestoviParser = (function () {
  const dajTacnost = function(parametar) {
    let lista=[];
    try{
       let niz=JSON.parse(parametar);
     var rezultat=(niz.stats.passes/niz.stats.tests)*100;
       var zaokruzi=Math.round(rezultat);
       if(zaokruzi!=rezultat) rezultat=rezultat.toFixed(1);
	   for(var e of niz.failures) {
		   lista.push(e.fullTitle);
	   }
    }catch(niz) {
           rezultat=0;
           lista.push("Testovi se ne mogu izvršiti");      
    }
     return {tacnost: rezultat.toString()+"%", greske: lista}
   }
    
   const porediRezultate = function(rezultat1, rezultat2) {
     let niz1=JSON.parse(rezultat1);
     let niz2=JSON.parse(rezultat2);
     let lista=[];
     let promjena;
    var jesu=true;
    var brojac=0;
    if(niz1.tests.length!=niz2.tests.length) jesu=false;
    for(var t1 of niz1.tests) {
      if(brojac==0 && jesu==false) {
        brojac=1;
        break;
      }
      jesu=false;
      for(var t2 of niz2.tests) {
            if(t1.fullTitle==t2.fullTitle) {
              jesu=true;
              break;
            }
       }
       if(jesu==false) break;
    }
    if(jesu==true) {
      promjena=(niz2.stats.passes/niz2.stats.tests)*100;
      for(var e of niz2.failures) {
        lista.push(e.fullTitle);
      }
      lista.sort(function(a,b) {
       if(a<b) return -1;
       if(a>b) return 1;
       return 0;
      })
      var zaokruzi=Math.round(promjena);
       if(zaokruzi!=promjena) promjena=promjena.toFixed(1);
      return {promjena: promjena.toString()+"%", greske: lista}
    }
    if(jesu==false) { 
    var brojTestovaPresjek=0; 
    var ima=false;
    for(var a of niz1.failures) {
      ima=false;
      for(var b of niz2.tests) {
        if(a.fullTitle==b.fullTitle) {
          ima=true; 
          break;
        }
      }
      if(ima==false) {
        brojTestovaPresjek=brojTestovaPresjek+1;
        lista.push(a.fullTitle);
     }
  }
  lista.sort(function(a,b) {
    if(a<b) return -1;
    if(a>b) return 1;
    return 0;
   })
   let lista1=[];
  for(var a of niz2.failures) {
    lista1.push(a.fullTitle);
  }
  lista1.sort(function(a,b) {
    if(a<b) return -1;
    if(a>b) return 1;
    return 0;
   })
   lista=lista.concat(lista1);
    promjena=((brojTestovaPresjek+niz2.failures.length)/(brojTestovaPresjek+niz2.tests.length))*100;
    var zaokruzi1=Math.round(promjena);
    if(zaokruzi1!=promjena) promjena=promjena.toFixed(1);
     return {promjena: promjena.toString()+"%", greske: lista}
   }
  }
    return {dajTacnost:dajTacnost, porediRezultate:porediRezultate}
} ());