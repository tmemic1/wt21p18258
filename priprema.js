const connection=require('./db');

const student = require("./Student");
const zadatak = require('./Zadaci');
const vjezba = require('./Vjezbe');
const grupa = require('./Grupe');
 
student.belongsTo(grupa,{foreignKey: 'grupa_id', sourceKey: 'id'});
vjezba.hasMany(zadatak,{foreignKey: 'vjezba_id', sourceKey: 'id'});

