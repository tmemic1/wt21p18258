const express=require('express');
const bodyParser = require('body-parser');
const fs=require('fs');
const app = express();
const connection=require('./db');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
//#region Otvaranje stranica
app.get('/prvi.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/prvi.html');
});
app.get('/vjezbe.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/vjezbe.html');
});
app.get('/mojRepozitorij.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/mojRepozitorij.html');
});
app.get('/prvi.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/prvi.html');
});
app.get('/zadaci.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/zadaci.html');
});
app.get('/zadatak2.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/zadatak2.html');
});
app.get('/zadatak4.html', function(req, res) {
	res.sendFile(__dirname + '/public/html/zadatak4.html');
});
    app.get('/unosVjezbi.html', function(req, res) {
        res.sendFile(__dirname + '/public/html/unosVjezbi.html');

});
app.use(express.static('public/html'));
app.use(express.static('public/css'));
app.use(express.static('public/js'));

  //#endregion

   async function dobaviVjezbe() {
    const upit='SELECT naziv from vjezba';  
    var brojvj=await connection.query(upit,{type: connection.QueryTypes.SELECT});
    return brojvj.length; 
   } 
   async function dobaviZadatke() {
     let niz=[];
     const upit1='SELECT brojzadataka from vjezba';
     var brojzad=await connection.query(upit1,{type: connection.QueryTypes.SELECT});
     for(let i=0;i<brojzad.length; i++) {
      niz.push(brojzad[i].brojzadataka);
      }
      return niz;
   }
   
  //#region GET metod
      app.get('/vjezbe/', async function(req, res)  {
        res.setHeader('Content-Type', 'application/json');
        var brojvez=await dobaviVjezbe();
        var brojz=await dobaviZadatke();
        
     res.json({brojVjezbi: brojvez, brojZadataka:brojz});
    });
    
    //#endregion

    //#region POST metod
    app.post('/vjezbe',async function(req,res){
        console.log(req.body['brojZadataka']);
        var nizGresaka =[];
        var stringGresaka="";
        let zadaci = req.body['brojZadataka'].toString().split(',');
        if(parseInt(req.body['brojVjezbi'])<1 || parseInt(req.body['brojVjezbi'])>15){
           nizGresaka.push("brojVjezbi");
        }
        for(let i=0;i<zadaci.length; i++){
            if(parseInt(zadaci[i])<0 || parseInt(zadaci[i])>10){
                nizGresaka.push("z"+i);
            }
        }
        if(req.body['brojVjezbi']!=zadaci.length){
            nizGresaka.push("brojZadataka");
        }

        for(let i=0;i<nizGresaka.length;i++){
            if(i!=nizGresaka.length-1){
                stringGresaka = stringGresaka +nizGresaka[i]+",";
            }
            else stringGresaka=stringGresaka +nizGresaka[i];
        }
        if(stringGresaka.length>0){
    res.send({status: "error",data: "Pograsan parametar "+stringGresaka});
    return;
        }
        let nizBrojeva=req.body['brojZadataka'].split(",");    
        for(let k=0; k<req.body['brojVjezbi']; k++) {
            pom=k;
            let vje="Vjezba "+(++pom);
            let upit3 = "INSERT INTO VJEZBA(naziv,brojzadataka)VALUES('" + vje + "'"+","+parseInt(nizBrojeva[k])+")";
          await connection.query(upit3,{type: connection.QueryTypes.INSERT});
          }
     });
    //#endregion
    
    //#region RUTE SA 4. SPIRALE
    app.post('/student', async function(req,res){
        let indekss=parseInt(req.body.index),postoji=0;
        let upit4='SELECT s.indeks FROM STUDENT s';       ///Dobavi sve indexe iz tabele student
        let broj=await connection.query(upit4,{type: connection.QueryTypes.SELECT});
        for(let s=0; s<broj.length; s++) {              ///Provjera ima da li vec postoji indeks
            if(broj[s].indeks==indekss) {
                indekss=broj[s].indeks;
                postoji=1;
                break;
            }
        }
        if(postoji==1) {    //Vrati status ako postoji
        res.send({status: "Student sa indexom {"+indekss+"} već postoji!"});
        return; 
        }   

        //Provjera ima li grupe sa datim nazivom
        let nazivGrupice=req.body.grupa;
        let upit5="SELECT id FROM grupa WHERE naziv="+"'"+nazivGrupice+"'";
       let x=await connection.query(upit5,{type: connection.QueryTypes.SELECT});
       let povratni;
       if(x.length==0) {
        let upit7="SELECT id FROM grupa";
        let x1=await connection.query(upit7,{type: connection.QueryTypes.SELECT});
        if(x1.length==0) {
            povratni=1;
        }else {
            povratni=x1.length+1;
        }
        let upit8="INSERT INTO GRUPA(naziv)values('"+nazivGrupice+"')";
        await connection.query(upit8,{type: connection.QueryTypes.INSERT});
       }else {
           povratni=x[0].id;
       }
       console.log("INDEKS JE: "+req.body.index);
        //Dodavanje studenta u tabelu Student
        let upit6="INSERT INTO STUDENT(ime,prezime,indeks,grupa_id)VALUES('"+req.body.ime+"'"+","+"'"+req.body.prezime+"'"+","+req.body.index+","+povratni+")";
          await connection.query(upit6,{type: connection.QueryTypes.INSERT});
          res.send({status:"Kreiran student!"});
    });

    app.put('/student/:index',async function(req,res){
      let indeksss=parseInt(req.params.index);
      let upitcic="SELECT s.indeks FROM STUDENT s WHERE s.indeks="+indeksss;
      let brojindeksa=await connection.query(upitcic,{type: connection.QueryTypes.SELECT});
      if(brojindeksa.length==0) {
          res.send({status:"Student sa indexom {"+indeksss+"} ne postoji"});
      }else {
         let upitZaGrupu="SELECT id FROM GRUPA WHERE naziv='"+req.body.grupa+"'"; 
         let ajdiGrupe=await connection.query(upitZaGrupu,{type: connection.QueryTypes.SELECT});
        let upitcic="UPDATE STUDENT SET grupa_id='"+ajdiGrupe[0].id+"'"+"WHERE indeks="+indeksss;  //MIJENJANJE GRUPE
        await connection.query(upitcic,{type: connection.QueryTypes.UPDATE});
          res.send({status:"Promjena grupa studentu {"+indeksss+"}"});
        }
    });
    //#endregion
app.listen(3000, () =>{
    console.log("Otvoren je port 3000");  
});