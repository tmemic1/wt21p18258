const Sequelize = require('sequelize');
const sequelize=require('./db');
    const Zadatak = sequelize.define('Zadatak', {
      naziv : Sequelize.STRING,
      vjezba_id: Sequelize.INTEGER
  },{
      freezeTableName: true
  },{
      timestamps: false
  });

  module.exports = Zadatak;

